#include "pch.h"
#include "scene.h"

Scene::Scene() {
	_node_cnt = 0;
}

void Scene::addNode(Node *node){
	if (!node)
		return;
	_map[_node_cnt].push_front(node);
	_sprite_index[node] = _node_cnt;
	_node_cnt++;
	
}

void Scene::addNode(Node *node, size_t id){
	if (!node)
		return;
	_sprite_index[node] = id;
	_map[id].push_front(node);
}

void Scene::draw(){
	for (auto& i : _map) {
		for (auto&j : i.second) {
			j->draw();
		}
	}
}

void Scene::update(){
	for (auto& i : _map) {
		for (auto&j : i.second) {
			j->update();
		}
	}
}

void Scene::destroy(Node *node){
	if (!node)
		return;
	int index = -1;
	for (auto i : _sprite_index) {
		if (i.first == node) {
			index = i.second;
		}
	}
	for(auto i=_map[index].begin();i!=_map[index].end();i++){
		if (*i == node) {
			_map[index].erase(i);
			break;
		}
	}
}
