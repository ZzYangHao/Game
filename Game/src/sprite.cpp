#include "pch.h"
#include "sprite.h"

Sprite::Sprite(std::string path){
	_path = sTows(path);
	loadimage(&_img, _path.c_str());

	_width = _img.getwidth();
	_height = _img.getheight();

	_anchor_x = 0;
	_anchor_y = 0;

	_x = 0;
	_y = 0;
	_nwidth = 1;
	_nheight = 1;

	_animation = NULL;
	_dwrop = SRCCOPY;
}

Sprite::~Sprite(){
	if (_animation)
		delete _animation;
}

void Sprite::addNode(Node *node){
	if (!node)
		return;
	_map[_node_cnt].push_front(node);
	_sprite_index[node]=_node_cnt;
	_node_cnt++;
}

void Sprite::addNode(Node *node, size_t id){
	if (!node)
		return;
	_sprite_index[node] = id;
	_map[id].push_front(node);
}

void Sprite::destroy(Node *node){
	if (!node)
		return;
	int index = -1;
	for (auto i : _sprite_index) {
		if (i.first == node) {
			index = i.second;
		}
	}
	for (auto i = _map[index].begin(); i != _map[index].end(); i++) {
		if (*i == node) {
			_map[index].erase(i);
			break;
		}
	}

}

void Sprite::draw(){
	int selfx = _x - _width * _anchor_x;
	int selfy = _y - _height * _anchor_y;
	putimage(selfx,selfy, _width*_nwidth, _height*_nheight,&_img,0,0,_dwrop);
	for (auto& i : _map) {
		for (auto& j : i.second) {
			int posx = j->_x + selfx;
			int posy = j->_y + selfy;
			j->draw(posx-j->_width*j->_anchor_x,posy-j->_anchor_y*j->_anchor_y);
		}
	}
}

void Sprite::update(){
	if (_isaction) {
		clearImage(_img);
		loadimage(&_img, _animation->next().c_str(), _width*_nwidth, _height*_nheight, true);
	}
	for (auto& i : _map) {
		for (auto&j : i.second) {
			j->update();
		}
	}
}

void Sprite::draw(int x, int y){
	putimage(x, y, &_img);
}

void Sprite::setPosition(int x, int y){
	_x = x;
	_y = y;
}

void Sprite::setAnchor(float x, float y){
	x = x < 0 ? 0 : x;
	y = y < 0 ? 0 : y;
	x = x > 1 ? 1 : x;
	y = y > 1 ? 1 : y;
	_anchor_x = x;
	_anchor_y = y;
}

void Sprite::setScaleX(float n){
	clearImage(_img);
	_nwidth = n;
	loadimage(&_img, _path.c_str(), _width*_nwidth, _height*_nheight, true);
}

void Sprite::setScaleY(float n){
	clearImage(_img);
	_nheight = n;
	loadimage(&_img, _path.c_str(), _width*_nwidth, _height*_nheight, true);
}

void Sprite::setDwRop(DWORD dwrop){
	_dwrop = dwrop;
}

void Sprite::runAction(Animation *action){
	_animation = new Animation(*action);
	_isaction = true;
}

void Sprite::clearImage(IMAGE & img){
	SetWorkingImage(&img);
	cleardevice();
	SetWorkingImage(NULL);
}
