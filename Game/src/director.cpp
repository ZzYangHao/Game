#include "pch.h"
#include "director.h"

Director * Director::_instance = NULL;

Director::Director(int x=800,int y=600) {
	_width = x;
	_height = y;
	_fps = 60;
	_isdebug = false;
}

Director * Director::getInstance(){
	if (!Director::_instance) {
		Director::_instance = new Director();
		return Director::_instance;
	}
	return Director::_instance;
}


void Director::destroy(){
	auto instance = Director::getInstance();
	if (!instance)
		return;
	delete instance;
	instance = NULL;
}

void Director::setDefaultScene(Scene * scene){
	_curScene = scene;
	_list.push_back(scene);
}

void Director::run(){
	if (!_curScene)
		return;
	if(_isdebug)
		initgraph(_width, _height, SHOWCONSOLE);
	else
		initgraph(_width, _height, NULL);
	while (1) {
		BeginBatchDraw();
		Sleep(1000 / _fps);
		update();
		cleardevice();
		draw();
		EndBatchDraw();
	}
}

void Director::draw(){
	_curScene->draw();
}

void Director::update(){
	_curScene->update();
}

void Director::addNode(Node *node){
	auto check = dynamic_cast<Scene*>(node);
	if (check) {
		_list.push_back(check);
	}
	else {
		return;
	}
}

void Director::addNode(Node *node, size_t){
	addNode(node);
}

void Director::destroy(Node *){

}

void Director::setDebugMode(bool){
	_isdebug = true;
}
