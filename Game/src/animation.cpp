#include "pch.h"
#include "animation.h"

Animation::Animation(){
	_index = 0;
	_isloop = false;
}

Animation::Animation(std::vector<std::string>&vec) {
	_index = 0;
	_isloop = false;
	_frames = std::move(vec);
}

Animation::Animation(Animation& copy) {
	_frames = std::move(copy._frames);
	_fps = copy._fps;
	_index = copy._index;
	_isloop = copy._isloop;
}


void Animation::addFrames(std::string frame){
	if (frame == "")
		return;
	_frames.push_back(frame);

}

void Animation::zeroIndex(){
	_index = 0;
}

bool Animation::hasNext(){
	if (_index < _frames.size())
		return true;
	if (_isloop&&_frames.size()>0) {
		_index = 0;
		return true;
	}
	else {
		return false;
	}
}

std::wstring Animation::next() {
	static int cnt = 0;
	if (hasNext()) {
		if (cnt == _fps) {
			cnt = 0;
			return sTows(_frames[_index++]);
		}
		else
			cnt++;
		return sTows(_frames[_index]);
	}
	return L"";
}

void Animation::setLoop(bool is){
	_isloop = is;
}

void Animation::setFps(size_t fps){
	_fps = fps;
}

