#include"pch.h"
#include"text.h"

Text::Text(std::string s) {
	_text = sTows(s);
	_color = 0xffffff;
	SetWorkingImage(NULL);
	gettextstyle(&_font);
}

void Text::draw(){
	settextcolor(_color);
	setbkmode(TRANSPARENT);
	settextstyle(&_font);
	outtextxy(_x, _y, _text.c_str());
	for (auto i : _map) {
		for (auto j : i.second) {
			j->draw();
		}
	}
}

void Text::setColor(COLORREF color){
	_color = color;
}

void Text::setWeight(float v){
	_font.lfWeight = v;
	settextstyle(&_font);
}

void Text::setWidth(long v){
	_font.lfWidth = v;
	settextstyle(&_font);
}

void Text::setHeight(long v){
	_font.lfHeight = v;
	settextstyle(&_font);
}

void Text::setFontName(std::string s){
	std::wstring tmp = sTows(s);
	_tcscpy_s(_font.lfFaceName, tmp.c_str());
	settextstyle(&_font);
}

void Text::setQuality(BYTE v){
	_font.lfQuality = v;
	settextstyle(&_font);
}

void Text::setItalic(BYTE v){
	_font.lfItalic = v;
	settextstyle(&_font);
}

void Text::setUnderline(BYTE v){
	_font.lfUnderline = v;
	settextstyle(&_font);
}

void Text::setStrikeOut(BYTE v){
	_font.lfStrikeOut = v;
	settextstyle(&_font);
}

void Text::setEscapement(long v){
	_font.lfEscapement = v;
	settextstyle(&_font);
}

