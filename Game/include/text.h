#ifndef TEXT_H
#define TEXT_H

#include"pch.h"
#include"sprite.h"



class Text :public Sprite {
public:
	Text(std::string);
	void draw();
	//void update();
	void setColor(COLORREF);
	void setWeight(float);
	void setWidth(long);
	void setHeight(long);
	void setFontName(std::string);
	void setQuality(BYTE);
	//斜体
	void setItalic(BYTE);
	//下划线
	void setUnderline(BYTE);
	//删除线
	void setStrikeOut(BYTE);
	//书写角度
	void setEscapement(long);

public:
	static const int _NONANTIALIASED_QUALITY = NONANTIALIASED_QUALITY;
	static const int _PROOF_QUALITY = PROOF_QUALITY;
	static const int _DRAFT_QUALITY = DRAFT_QUALITY;
	static const int _DEFAULT_QUALITY = DEFAULT_QUALITY;
	static const int _ANTIALIASED_QUALITY=ANTIALIASED_QUALITY;

public:
	std::wstring _text;
	LOGFONT _font;
	COLORREF _color;
	static struct COLORS {
		static const COLORREF	_BLACK = 0;
		static const COLORREF	_BLUE = 0xAA0000;
		static const COLORREF	_GREEN = 0x00AA00;
		static const COLORREF	_CYAN = 0xAAAA00;
		static const COLORREF	_RED = 0x0000AA;
		static const COLORREF	_MAGENTA = 0xAA00AA;
		static const COLORREF	_BROWN = 0x0055AA;
		static const COLORREF	_LIGHTGRAY = 0xAAAAAA;
		static const COLORREF	_DARKGRAY = 0x555555;
		static const COLORREF	_LIGHTBLUE = 0xFF5555;
		static const COLORREF	_LIGHTGREEN = 0x55FF55;
		static const COLORREF	_LIGHTCYAN = 0xFFFF55;
		static const COLORREF	_LIGHTRED = 0x5555FF;
		static const COLORREF	_LIGHTMAGENTA = 0xFF55FF;
		static const COLORREF	_YELLOW = 0x55FFFF;
		static const COLORREF	_WHITE = 0xFFFFFF;
	};
};

#endif // !TEXT_H

