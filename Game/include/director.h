#ifndef DIRECTOR_H
#define DIRECTOR_H

#include"pch.h"
#include"node.h"
#include"scene.h"

class Director:public Node {
public:
	static Director* getInstance();
	static void destroy();
public:
	void setDefaultScene(Scene *);
	void run();
	void draw();
	void update();
	void addNode(Node *);
	void addNode(Node *, size_t);
	void destroy(Node*);

	void setDebugMode(bool);


private:
	Director(int,int);
	int _fps;
	bool _isdebug;

private:
	static Director *_instance;
	std::list<Scene*>_list;
	Scene *_curScene;

	int _width, _height;
};

#endif // !DIRECTOR_H