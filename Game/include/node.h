#ifndef NODE_H
#define NODE_H

class Node {
public:
	virtual void addNode(Node*) = 0;
	virtual void addNode(Node*,size_t) = 0;
	virtual void draw() = 0;
	virtual void update() = 0;
	virtual void destroy(Node*) {};
	virtual void draw(int, int) {};

public:
	std::map<int, std::list<Node*>>_map;
	std::map<Node*, int>_sprite_index;
	int _x, _y;
	float _anchor_x, _anchor_y;
	int _width, _height;
	size_t _node_cnt;
};

#endif // !NODE_H