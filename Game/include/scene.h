#ifndef SCENE_H
#define SCENE_H

#include"node.h"
#include"sprite.h"

class Scene :public Node {
public:
	Scene();
	virtual void addNode(Node*);
	void addNode(Node*, size_t);
	virtual void draw();
	virtual void update();
	void destroy(Node*);

};

#endif // !SCENE_H

