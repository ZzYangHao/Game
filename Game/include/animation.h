#ifndef ANIMATION_H
#define ANIMATION_H

#include"pch.h"

class Animation {
public:
	Animation();
	Animation(std::vector<std::string>&);
	Animation(Animation&);

	void addFrames(std::string);
	void zeroIndex();
	bool hasNext();
	std::wstring next();
	void setLoop(bool);
	void setFps(size_t);

private:
	std::vector<std::string>_frames;
	int _index;
	size_t _fps;
	bool _isloop;
};

#endif // !ANIMATION_H

