#ifndef SPRITE_H
#define SPRITE_H
#include"pch.h"
#include"node.h"
#include"animation.h"


class Sprite : public Node {

public:
	Sprite(std::string="");
	
	virtual ~Sprite();

	void addNode(Node*);
	void addNode(Node*,size_t);
	void destroy(Node*);

	virtual void draw();
	virtual void update();
	
	void draw(int, int);
	void setPosition(int, int);
	void setAnchor(float, float);
	void setScaleX(float);
	void setScaleY(float);
	void setDwRop(DWORD);

	void runAction(Animation *action);
	

private:
	void clearImage(IMAGE &img);
public:
	float _nwidth, _nheight;
	IMAGE _img;
	std::wstring _path;

public:
	bool _isaction;
private:
	Animation *_animation;
	DWORD _dwrop;
};

#endif // !SPRITE_H
